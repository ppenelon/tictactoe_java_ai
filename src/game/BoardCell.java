package game;

public enum BoardCell {
	CIRCLE,
	CROSS,
	NOTHING;

	public static String toString(BoardCell bc){
		return (bc == NOTHING ? " " : (bc == CIRCLE ? "O" : "X"));
	}
}
