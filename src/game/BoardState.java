package game;

import math.geom.Vector2;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static game.BoardCell.NOTHING;
import static game.BoardCell.CROSS;
import static game.BoardCell.CIRCLE;

public class BoardState {

	protected BoardCell[] state;

	public BoardState(){
		this.state = new BoardCell[]{
			NOTHING, NOTHING, NOTHING,
			NOTHING, NOTHING, NOTHING,
			NOTHING, NOTHING, NOTHING
		};
	}

	public BoardState(BoardState bs){
		this.state = new BoardCell[]{
			bs.state[0], bs.state[1], bs.state[2],
			bs.state[3], bs.state[4], bs.state[5],
			bs.state[6], bs.state[7], bs.state[8]
		};
	}

	public void set(int x, int y, BoardCell boardCell){
		this.state[(y * 3) + x] = boardCell;
	}

	public BoardCell get(int x, int y){
		if(x < 0) x = 0;
		if(x > 2) x = 2;
		if(y < 0) y = 0;
		if(y > 2) y = 2;
		return this.state[(y * 3) + x];
	}

	public BoardCell checkWin(){
		//Vérification des lignes
		for (int i = 0; i < 3; i++) {
			if(this.state[(3 * i) + 0] == this.state[(3 * i) + 1] && this.state[(3 * i) + 1] == this.state[(3 * i) + 2] && this.state[(3 * i) + 0] != NOTHING)
				return this.state[(3 * i) + 0];
		}
		//Vérification des colonnes
		for (int i = 0; i < 3; i++) {
			if(this.state[(3 * 0) + i] == this.state[(3 * 1) + i] && this.state[(3 * 1) + i] == this.state[(3 * 2) + i] && this.state[(3 * 0) + i] != NOTHING)
				return this.state[(3 * 0) + i];
		}
		//Verification des diagonales
		if(this.state[0] == this.state[4] && this.state[4] == this.state[8] && this.state[0] != NOTHING)
			return this.state[0];
		if(this.state[2] == this.state[4] && this.state[4] == this.state[6] && this.state[2] != NOTHING)
			return this.state[2];
		//Aucun winner
		return NOTHING;
	}

	public void reset(){
		for (int i = 0; i < this.state.length; i++) {
			this.state[i] = NOTHING;
		}
	}

	public void display(){
		System.out.println(" --- BOARD STATE --- ");
		System.out.println(BoardCell.toString(this.state[0]) + BoardCell.toString(this.state[1]) + BoardCell.toString(this.state[2]));
		System.out.println(BoardCell.toString(this.state[3]) + BoardCell.toString(this.state[4]) + BoardCell.toString(this.state[5]));
		System.out.println(BoardCell.toString(this.state[6]) + BoardCell.toString(this.state[7]) + BoardCell.toString(this.state[8]));
		System.out.println(" --- BOARD STATE --- ");
	}

	public Vector2[] playableCells(){
		ArrayList<Vector2> playableCells = new ArrayList<>();
		for (int i = 0; i < this.state.length; i++) {
			if(this.state[i] == NOTHING)
				playableCells.add(new Vector2(i % 3, (int) Math.floor(i / 3)));
		}
		return playableCells.toArray(new Vector2[playableCells.size()]);
	}
}
