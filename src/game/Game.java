package game;

import ai.GameTreeAi;
import input.MouseController;
import math.geom.Vector2;
import third_party.StdDraw;

import java.awt.*;

public class Game {

	private static final float FRAMERATE = 60f;
	private static final Color BACKGROUND_COLOR = Color.WHITE;

	public MouseController mouseController;

	public int width;
	public int height;

	public Board board;
	public GameTreeAi ai;

	public Game(int width, int height, String title){
		StdDraw.setCanvasSize(width, height);
		StdDraw.setXscale(0, width);
		StdDraw.setYscale(height, 0);
		StdDraw.getFrame().setTitle(title);
		StdDraw.enableDoubleBuffering();

		this.mouseController = new MouseController();

		this.width = width;
		this.height = height;

		this.init();
	}
	public void run(){
		while(true){
			StdDraw.clear(BACKGROUND_COLOR);
			update();
			draw();
			StdDraw.show();
			StdDraw.pause((int) ((1 / FRAMERATE) * 1000));
		}
	}

	public void init(){
		this.board = new Board(this);
		this.ai = new GameTreeAi(board);
	}

	private void update(){
		mouseController.update();
		if(mouseController.isJustPressed){
			//Coup du joueur
			Vector2 position = new Vector2(
				(float) Math.floor(mouseController.position.x / (width / 3f)),
				(float) Math.floor(mouseController.position.y / (height / 3f))
			);
			//On fait jouer le joueur et on regarde si le coup est valide
			if(board.play(position) && !checkEndGame()){
				//L'IA joue
				ai.play();
				//On vérifie encore que la partie n'est pas terminée
				checkEndGame();
			}

		}
	}

	private boolean checkEndGame(){
		//Vérification d'un gagnant
		BoardCell winner = board.checkWin();
		if(winner != BoardCell.NOTHING) {
			System.out.println((winner == BoardCell.CIRCLE ? "O" : "X") + " wins !");
			board.reset();
			return true;
		}
		//Vérification d'une partie nulle
		if(board.playableCells().length == 0){
			System.out.println("Partie nulle !");
			board.reset();
			return true;
		}
		//Sinon la partie n'est pas terminée
		return false;
	}

	private void draw(){
		this.board.draw();
	}

	public float pixelToPercent(){
		return 1f / height;
	}
}
