package game;

import math.geom.Vector2;
import math.geom.shape.Circle;
import math.geom.shape.Cross;
import math.geom.shape.Line;
import math.geom.shape.Shape;

import java.awt.*;
import java.util.HashMap;

import static game.BoardCell.CIRCLE;
import static game.BoardCell.CROSS;
import static game.BoardCell.NOTHING;

public class Board extends BoardState {

	private Game game;

	private BoardCell turn;

	private Line[] boardDrawing;
	private HashMap<Integer, Shape> boardShapes;

	public Board(Game gameInstance){
		super();
		this.turn = CIRCLE;

		this.game = gameInstance;
		this.boardDrawing = new Line[]{ //Les lignes du plateau
			new Line(new Vector2(0, 400f / 3f), 90f, 400f),
			new Line(new Vector2(0, 2 * (400f / 3f)), 90f, 400f),
			new Line(new Vector2(400f / 3f, 0), 180f, 400f),
			new Line(new Vector2(2 * (400f / 3f), 0), 180f, 400f),
		};
		this.boardShapes = new HashMap<>();
	}

	public void draw(){
		//Dessin du plateau
		for(Line l : boardDrawing){
			l.draw(Color.BLACK, game.pixelToPercent() * 5f);
		}
		//Dessin du jeu
		for(Shape shape : boardShapes.values()){
			shape.draw(Color.BLACK, game.pixelToPercent() * 5f);
		}
	}

	/**
	 * Fait jouer le joueur et retourne un booleen indiquant si le joueur a bien joué
	 */
	public boolean play(Vector2 position){
		//On converti la position en clé dans le tableau
		int key = (int) ((position.y * 3) + position.x);

		//Si on clique sur une case déjà occupée on ne fait rien
		if(super.get((int)position.x, (int)position.y) != NOTHING)
			return false;

		//On place le symbole virtuel sur le plateau
		super.set((int)position.x, (int)position.y, this.turn);

		//On place le symbole visuel sur le plateau
		if(this.turn == CROSS){
			this.boardShapes.put(
				key,
				new Cross(new Vector2(
					((game.width / 3f) * position.x) + (game.width / 6f),
					((game.height / 3f) * position.y) + (game.height / 6f)
				), (game.width / 6f) - 10f)
			);
		}
		else if(this.turn == CIRCLE){
			this.boardShapes.put(
				key,
				new Circle(new Vector2(
					((game.width / 3f) * position.x) + (game.width / 6f),
					((game.height / 3f) * position.y) + (game.height / 6f)
				), (game.width / 6f) - 15f)
			);
		}

		//On change la personne qui doit jouer
		this.turn = (this.turn == CIRCLE) ? CROSS : CIRCLE;

		return true;
	}

	public void reset(){
		super.reset();
		this.boardShapes.clear();
		this.turn = CIRCLE;
	}

	public BoardCell getTurn() {
		return turn;
	}
}
