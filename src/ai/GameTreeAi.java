package ai;

import game.Board;
import game.BoardCell;
import game.BoardState;
import math.geom.Vector2;

public class GameTreeAi {

	private static final int[] WIN_POINTS = new int[]{5000000, 300000, 10000, 1000, 500, 100, 10, 1};
	private static final int[] LOOSE_POINTS = new int[]{-5000000, -300000, -10000, -1000, -500, -100, -10, -1};
	private static final int[] DRAW_POINTS = new int[]{0, 0, 0, 0, 0, 0, 0, 0};

	private Board boardInstance;

	public GameTreeAi(Board boardInstance) {
		this.boardInstance = boardInstance;
	}

	public void play(){
		BoardState boardStateCopy = new BoardState(boardInstance);
		Vector2 bestPosition = null;
		int bestPositionScore = 0;
		for(Vector2 playablePosition : boardInstance.playableCells()){
			int positionScore = deepCourseFromPosition(playablePosition, boardInstance.getTurn(), 0, boardStateCopy);
			if(bestPosition == null || positionScore > bestPositionScore){
				bestPosition = playablePosition;
				bestPositionScore = positionScore;
			}
		}
		boardInstance.play(bestPosition);
	}

	private int deepCourseFromPosition(Vector2 playedPosition, BoardCell turn, int playIndex, BoardState bs){
		bs.set((int) playedPosition.x, (int) playedPosition.y, turn);

		BoardCell winner = bs.checkWin();

		int score = 0;

		if(winner != BoardCell.NOTHING){
			score = (winner == boardInstance.getTurn()) ? WIN_POINTS[playIndex] : LOOSE_POINTS[playIndex];
		}
		else if(bs.playableCells().length == 0){
			score = DRAW_POINTS[playIndex];
		}
		else{
			for(Vector2 playableCell : bs.playableCells()){
				score += deepCourseFromPosition(playableCell, turn == BoardCell.CIRCLE ? BoardCell.CROSS : BoardCell.CIRCLE, playIndex + 1, bs);
			}
		}

		bs.set((int) playedPosition.x, (int) playedPosition.y, BoardCell.NOTHING);

		return score;
	}
}
