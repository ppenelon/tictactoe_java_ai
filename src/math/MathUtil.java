package math;

public class MathUtil {

	public static final float PI = 3.14159265359f;

	public static float sigmoid(float coef){
		coef = (coef * 2f) - 1f;
		return 1f / (1f + (float) Math.exp(-5f * coef));
	}

	public static float clamp(float min, float value, float max){
		return Math.max(min, Math.min(max, value));
	}

	public static float degToRad(float deg){
		return deg * (PI / 180f);
	}

	public static float radToDeg(float rad){
		return rad / (PI / 180f);
	}

	public static void main(String[] args) {
		System.out.println(clamp(0, 1, 2)); // 1
		System.out.println(clamp(1, 0, 2)); // 1
		System.out.println(clamp(0, 2, 1)); // 1
	}
}
