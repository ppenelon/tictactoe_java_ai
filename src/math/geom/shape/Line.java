package math.geom.shape;

import math.geom.Vector2;
import third_party.StdDraw;

import java.awt.*;

public class Line extends Shape {

	public Vector2 endPosition;

	public Line(Vector2 startPosition, Vector2 endPosition){
		super(startPosition);
		this.endPosition = new Vector2(endPosition);
	}

	public Line(Vector2 startPosition, float rotation, float lineSize){
		super(startPosition);
		this.endPosition = Vector2.degToNorm(rotation).mult(lineSize).add(startPosition);
	}

	@Override
	public void draw(Color color, float lineWidth){
		StdDraw.line(position.x, position.y, endPosition.x, endPosition.y);
	}

}
