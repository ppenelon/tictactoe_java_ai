package math.geom.shape;

import math.geom.Vector2;
import third_party.StdDraw;

import java.awt.*;

public class Circle extends Shape {

	public float radius; //Rayon du cercle

	public Circle(Vector2 position, float radius){
		super(position);
		this.radius = radius;
	}

	/**
	 * Si lineWidth < 0 alors le cercle est plein
	 */
	@Override
	public void draw(Color color, float lineWidth) {
		super.draw(color, lineWidth);
		if(lineWidth < 0)
			StdDraw.filledCircle(position.x, position.y, radius);
		else
			StdDraw.circle(position.x, position.y, radius);
	}
}
