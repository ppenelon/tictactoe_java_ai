package math.geom.shape;

import math.geom.Vector2;
import third_party.StdDraw;

import java.awt.*;

public abstract class Shape {

	public Vector2 position;

	protected Shape(Vector2 position){
		this.position = new Vector2(position);
	}

	public void draw(Color color, float lineWidth){
		StdDraw.setPenColor(color);
		if(lineWidth >= 0)
			StdDraw.setPenRadius(lineWidth / 2f);
	}
}
