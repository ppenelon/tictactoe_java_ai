package math.geom.shape;

import math.geom.Vector2;

import java.awt.*;

public class Cross extends Shape {

	public Vector2 position;
	private Line[] lines;

	public Cross(Vector2 position, float width){
		super(position);
		this.lines = new Line[]{
			new Line(position, 45, width),
			new Line(position, 135, width),
			new Line(position, 225, width),
			new Line(position, 315, width),
		};
	}

	@Override
	public void draw(Color color, float lineWidth) {
		super.draw(color, lineWidth);
		for(Line l : lines)
			l.draw(color, lineWidth);
	}
}
