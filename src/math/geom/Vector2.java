package math.geom;

import math.MathUtil;

public class Vector2 {

	public float x;
	public float y;

	public Vector2(){
		this.x = 0f;
		this.y = 0f;
	}
	public Vector2(float x, float y){
		this.set(x, y);
	}
	public Vector2(Vector2 v){
		this.set(v);
	}

	public void set(Vector2 v){
		this.set(v.x, v.y);
	}
	public void set(float x, float y){
		this.x = x;
		this.y = y;
	}
	public void set(float v){
		this.x = v;
		this.y = v;
	}

	public Vector2 add(Vector2 v){
		return new Vector2(
			this.x + v.x,
			this.y + v.y
		);
	}
	public Vector2 add(float x, float y){
		return new Vector2(
			this.x + x,
			this.y + y
		);
	}
	public Vector2 add(float v){
		return new Vector2(
			this.x + v,
			this.y + v
		);
	}

	public Vector2 remove(Vector2 v){
		return new Vector2(
			this.x - v.x,
			this.y - v.y
		);
	}
	public Vector2 remove(float x, float y){
		return new Vector2(
			this.x - x,
			this.y - y
		);
	}
	public Vector2 remove(float v){
		return new Vector2(
			this.x - v,
			this.y - v
		);
	}

	public Vector2 mult(Vector2 v){
		return new Vector2(
			this.x * v.x,
			this.y * v.y
		);
	}
	public Vector2 mult(float x, float y){
		return new Vector2(
			this.x * x,
			this.y * y
		);
	}
	public Vector2 mult(float v){
		return new Vector2(
			this.x * v,
			this.y * v
		);
	}

	public Vector2 div(Vector2 v){
		return new Vector2(
			this.x / v.x,
			this.y / v.y
		);
	}
	public Vector2 div(float x, float y){
		return new Vector2(
			this.x / x,
			this.y / y
		);
	}
	public Vector2 div(float v){
		return new Vector2(
			this.x / v,
			this.y / v
		);
	}

	public float toRad(){
		return (float) Math.atan2(this.y, this.x);
	}
	public float toDeg(){
		return MathUtil.radToDeg(toRad());
	}

	public String toString(){
		return "(" + this.x + ", " + this.y + ")";
	}

	/**
	 * Transforme une valeur en degrées en un Vector2 normalisé
	 */
	public static Vector2 degToNorm(float deg){
		float rad = MathUtil.degToRad(deg);
		return new Vector2(
			(float) Math.sin(rad),
			(float) -Math.cos(rad)
		);
	}
}
