package input;

import math.geom.Vector2;
import third_party.StdDraw;

public class MouseController {

	public Vector2 position;

	public boolean isMousePressed;
	public boolean isMouseReleased;

	public boolean isJustPressed;
	public boolean isJustReleased;

	public MouseController(){
		this.position = new Vector2();
		this.isMousePressed = false;
		this.isMouseReleased = false;
		this.isJustPressed = false;
		this.isJustReleased = false;
	}

	public void update(){
		this.position.set((float) StdDraw.mouseX(), (float) StdDraw.mouseY());

		boolean wasPressed = this.isMousePressed;
		this.isMousePressed = StdDraw.isMousePressed();
		this.isJustPressed = !wasPressed && this.isMousePressed;

		boolean wasReleased = this.isMouseReleased;
		this.isMouseReleased = !StdDraw.isMousePressed();
		this.isJustReleased = !wasReleased && this.isMouseReleased;

	}
}
